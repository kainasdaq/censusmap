package neu.KaiDeng;

import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class myDB {
	final String DB_NAME = "DB_Census";
	final String TBL_NAME = "TBL_Census";
	final String CREATE_TABLE = "create table if not exists " + TBL_NAME + "("
			+ "ID integer PRIMARY KEY autoincrement, "
			+ "statePostal text, "
			+ "placenameFull text, "
			+ "lng real, "
			+ "lat real, "
			+ "pop real, "
			+ "totSqMi real, "
			+ "popSqMi real, "
			+ "pctHisp real, "
			+ "pctWhite real, "
			+ "pctBlack real, "
			+ "pctAsian real); ";
	final String INSERT_QUERY = "insert into " + TBL_NAME
			+ "(statePostal, placenameFull, lng, lat, pop, totSqMi, popSqMi, pctHisp, pctWhite, pctBlack, pctAsian)" 
			+ " values (";
	
	final String[] columns = {"statePostal", "placenameFull", "lng", "lat", "pop", 
			"totSqMi", "popSqMi", "pctHisp", "pctWhite", "pctBlack", "pctAsian" };
	
	SQLiteDatabase db;	
	String selectedState = "";
	
	public myDB(String state){
		selectedState = state;		
	}
	
	/******************* DATABASE METHODS START *********************/
	public  List<String> processQuery(){
		//introTxt.setText("Querying the local database...");
		//showWaiting();
		List<String> finalList = new ArrayList<String>();
    	openDB();
    	finalList = queryDB("statePostal='"+selectedState+"'" , null);
    	closeDB();
    	return  finalList;
    	//showList();
    	//introTxt.setText("Records found in the local database" + "\nPress menu for options");
	}
	
	public void openDB() {
		try {
			String storagePath = Environment.getExternalStorageDirectory().getPath();
			String dbPath = storagePath + "/" + DB_NAME;
			db = SQLiteDatabase.openOrCreateDatabase(dbPath, null);
		} catch (SQLException e) {
			//Toast.makeText(this, "openDB: "+e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	
	public void closeDB(){
		db.close();
	}
	
	public void createTable() {
		try {
			db.execSQL(CREATE_TABLE);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/*
	private void resetDB() {
		try {
			dropTable();
			createTable();
		} catch (SQLException e) {
			//createTable();
		}
	}*/
	
	public void dropTable() {
		try {
			db.execSQL("DROP TABLE IF EXISTS " + TBL_NAME);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteRecords(){
    	try {
			db.execSQL("DELETE FROM " + TBL_NAME + " WHERE statePostal='" + selectedState + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
	
	public void insertList(List<Census> censusList) {
		try {
			for (int i = 0; i < censusList.size(); i++){
				Census tmp_c = censusList.get(i);
				db.execSQL(INSERT_QUERY + "'" + selectedState + "','" + tmp_c.placenameFull + "'," + tmp_c.lng + "," 
						+ tmp_c.lat + "," + tmp_c.pop + "," + tmp_c.totSqMi + "," 
						+ tmp_c.popSqMi + "," + tmp_c.pctHisp + "," + tmp_c.pctWhite + "," 
						+ tmp_c.pctBlack + "," + tmp_c.pctAsian + ")");
			}
		} catch (SQLException e) {
			//Toast.makeText(context, "Insert: " + e.getMessage(), Toast.LENGTH_LONG).show();
			System.out.println("insert: " + e.getMessage());
		} 
	}
	
	public List<String> queryDB(String where, String orderBy) {
		List<String> finalList = new ArrayList<String>();
		//finalList.clear();
		finalList.add("ListSize");		
		int i = 1;
		try {
			Cursor c = db.query(TBL_NAME, columns, where, null, null, null, orderBy);
			c.moveToFirst();
			while (!c.isAfterLast()) {
				String temp = i + ". "+ c.getString(1) + "\n" 
						+ "\t Pop: " + c.getString(4) + "\t\t TotalSqMi: " + c.getString(5);
				finalList.add(temp);
				c.moveToNext();
				i++;
			}			
			String note = "Found " + String.valueOf(finalList.size()-1) 
					+ " records in the "+ selectedState + ": \n";
			finalList.set(0, note);
		} catch (Exception e) {
			String note = "No table has been found";
			finalList.set(0, note);
			e.printStackTrace();
			System.out.println("query: " + e.getMessage());
		} 
		
		return finalList;
	}
	
	public boolean checkStateExisting(String state) {
		try {
			String query = "SELECT CASE WHEN EXISTS " +
					"(SELECT statePostal FROM "+TBL_NAME+" WHERE statePostal='"+state + "') " 
					+"THEN 'true' ELSE 'false' END";
			Cursor c = db.rawQuery(query, null);
			c.moveToFirst();
			//System.out.println("cursor: " + c.getString(0));
			
			if(c.getString(0).equals("true")){
				return true;
			}else{
				return false;
			}
			
		} catch (Exception e) {
			System.out.println("query: " + e.getMessage());
			e.printStackTrace();			
		}

		return false;	
	}
	
	public Census getCityInfo(String city){
		Census cityInfo = new Census();
		String where = "placenameFull like '" + city + "'";
		try {
			Cursor c = db.query(TBL_NAME, columns, where, null, null, null, null);
			c.moveToFirst();
			
			cityInfo.statePostal = c.getString(0);
			cityInfo.placenameFull = c.getString(1);
			cityInfo.lng = c.getString(2);
			cityInfo.lat = c.getString(3);
			cityInfo.pop = c.getString(4);
			cityInfo.totSqMi = c.getString(5);
			cityInfo.popSqMi = c.getString(6);
			cityInfo.pctHisp = c.getString(7);
			cityInfo.pctWhite = c.getString(8);
			cityInfo.pctBlack = c.getString(9);
			cityInfo.pctAsian = c.getString(10);
						
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("getCityInfo: " + e.getMessage());
		} 
		return cityInfo;		
	}
	
	public WeakHashMap<String, Census> getAroundCities(double lng, double lat){
		WeakHashMap<String, Census> cityMap = new WeakHashMap<String, Census>();
		double shiftV = 0.2;
		double e_lng = lng + shiftV;
		double w_lng = lng - shiftV;
		double n_lat = lat + shiftV;
		double s_lat = lat - shiftV;
		String where = "lng < " + e_lng + " AND lng > " + w_lng + 
				" AND lat < " + n_lat + " AND lat > " + s_lat;
		try {
			Cursor c = db.query(TBL_NAME, columns, where, null, null, null, null);
			c.moveToFirst();
			
			while (!c.isAfterLast()) {
				Census cityInfo = new Census();
				
				cityInfo.statePostal = c.getString(0);
				cityInfo.placenameFull = c.getString(1);
				cityInfo.lng = c.getString(2);
				cityInfo.lat = c.getString(3);
				cityInfo.pop = c.getString(4);
				cityInfo.totSqMi = c.getString(5);
				cityInfo.popSqMi = c.getString(6);
				cityInfo.pctHisp = c.getString(7);
				cityInfo.pctWhite = c.getString(8);
				cityInfo.pctBlack = c.getString(9);
				cityInfo.pctAsian = c.getString(10);
				cityMap.put(cityInfo.placenameFull + ", " + cityInfo.statePostal, cityInfo);
				
				c.moveToNext();
			}						
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("getAroundCities: " + e.getMessage());
		} 
		return cityMap;		
	}
	/******************* DATABASE MOTHODS END *********************/

}
