package neu.KaiDeng;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.WeakHashMap;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.GoogleMap.*;
import com.google.android.gms.maps.model.*;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MyMapActivity extends Activity {
	private Intent intent;
	private Bundle bundle;
	private TextView cityTxt;
	private String selectedState;
	private String city;
	private Census cityInfo;
	
	private HashMap<String, String> mMarkers;
	private WeakHashMap<String,Census> cityMap;
	private myDB databaseAdapter;
	private DecimalFormat df;
	private GoogleMap map;

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		
		df = (DecimalFormat) NumberFormat.getInstance();
		df.applyPattern("##.##%");
		mMarkers = new HashMap<String, String>();
		cityMap = new WeakHashMap<String, Census>();
		
		intent = this.getIntent();
		bundle = intent.getExtras();
		selectedState = bundle.getString("state");
		city = bundle.getString("city");	
		LatLng ctrLoc = getAroundCities(city);
		
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();	
		// MAP_TYPE_SATELLITE, MAP_TYPE_NORMAL, MAP_TYPE_HYBRID, or MAP_TYPE_TERRAIN
		map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		// map.setMyLocationEnabled(true);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(ctrLoc, 13));
		map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
			
		map.setOnInfoWindowClickListener(new  OnInfoWindowClickListener() {
			
			public void onInfoWindowClick(Marker marker) {
				// TODO Auto-generated method stub						
				String details = mMarkers.get(marker.getId());				

				AlertDialog.Builder dialog = new AlertDialog.Builder(MyMapActivity.this);

				dialog.setPositiveButton("Back", new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}

				});
				dialog.setTitle(marker.getTitle());
				dialog.setMessage(details);
				dialog.show();		
			}
		});
		
		// add all markers
		addMarkers(map, ctrLoc);
		makeToast("Click on a pin marker for more details");	
	}
	
	private LatLng getAroundCities(String c){
		databaseAdapter = new myDB(selectedState);
		databaseAdapter.openDB();
		cityInfo = databaseAdapter.getCityInfo(c);
		double lng = Double.valueOf(cityInfo.lng);
		double lat = Double.valueOf(cityInfo.lat);
		cityMap = databaseAdapter.getAroundCities(lng, lat);
		databaseAdapter.closeDB();
		
		int cities = cityMap.size() - 1;
		String bigTitle = cityInfo.placenameFull + ", " + cityInfo.statePostal;
		if (cities == 1) {
			bigTitle += " and another city";
		} else if (cities >= 2) {
			bigTitle = cityInfo.placenameFull + ", " + cityInfo.statePostal
					+ " and other " + cityMap.size() + " cities";
		}
		cityTxt = (TextView)findViewById(R.id.cityTxt);
		cityTxt.setText(bigTitle);
		
		return getCtrLatLng(lat, lng);
	}

	private LatLng getCtrLatLng(double lat, double lng) {
		LatLng ret;
		ret = new LatLng(lat, lng);
		return ret;
	}

	private void addMarkers(GoogleMap m, LatLng ctr) {
		LatLng cityLoc;

		for (String key : cityMap.keySet()) {// key is Census.placenameFull + ", " + Census.statePostal

			Census tmpCity = cityMap.get(key);
			String title = tmpCity.placenameFull + ", " + tmpCity.statePostal;
			String snippet = "Population: " + tmpCity.pop + "\n"
					+ "Total SqMi: " + tmpCity.totSqMi + "\n";
			
			String pctHisp = df.format(Double.valueOf(tmpCity.pctHisp));
			String pctWhite = df.format(Double.valueOf(tmpCity.pctWhite));
			String pctBlack = df.format(Double.valueOf(tmpCity.pctBlack));
			String pctAsian = df.format(Double.valueOf(tmpCity.pctAsian));

			String details = "Latitude: " + tmpCity.lat + "\n" + "Longtitude: "
					+ tmpCity.lng + "\n" + "Population: " + tmpCity.pop + "\n"
					+ "Total SqMi: " + tmpCity.totSqMi + "\n"
					+ "Pop per SqMi: " + tmpCity.popSqMi + "\n" + "Hispanic: "
					+ pctHisp + "\n" + "Caucasian: " + pctWhite + "\n"
					+ "African: " + pctBlack + "\n" + "Asian: " + pctAsian
					+ "\n";

			double tmpLng = Double.valueOf(tmpCity.lng);
			double tmpLat = Double.valueOf(tmpCity.lat);

			cityLoc = new LatLng(tmpLat, tmpLng);

			Marker marker = m.addMarker(new MarkerOptions()
							.position(cityLoc)
							.title(title)
							.alpha(0.8f)
							.snippet(snippet)
							.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.pin)));

			if (cityLoc.equals(ctr)){
				marker.showInfoWindow();
			}
			
			mMarkers.put(marker.getId(), details);
		}
	}

	private void makeToast(String s) {
		Toast toast = Toast.makeText(getApplicationContext(), s,
				Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	/******************* MENU STARTS *********************/
	public boolean onCreateOptionsMenu(Menu m) {
		int groupId = 0;
		int order = 0;
		int itemId = 0;
		m.add(groupId, itemId++, order, " << Back");
		m.add(groupId, itemId++, order, "Exit");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:// back
			onBackPressed();
			break;
		case 1:// exit app
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setPositiveButton("YES", new OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					setResult(Activity.RESULT_CANCELED, intent);
					finish();
				}

			});

			dialog.setNegativeButton("NO", new OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}

			});

			dialog.setTitle("Exit Application");
			dialog.setMessage("Are you sure you want to exit?");
			dialog.show();
			break;
		}
		return true;
	}

	/******************* MENU ENDS *********************/

	public void onBackPressed() {
		setResult(Activity.RESULT_OK, intent);
		finish();
	}

}
