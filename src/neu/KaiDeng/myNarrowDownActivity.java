package neu.KaiDeng;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class myNarrowDownActivity extends Activity{
	Intent intent;
	Bundle bundle;
	String selectedState;
	
	TextView introTxt;
	EditText editState;
	EditText editPop;
	Button searchBtn;
	Button cancelBtn;
	Button exitBtn;
	RadioGroup radioGroup;
	RadioButton selectedRadio;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.narrow_down);
		
		intent = this.getIntent();
		bundle = intent.getExtras();
		selectedState = bundle.getString("state");
		
		introTxt = (TextView)findViewById(R.id.introTxt);
		editState = (EditText)findViewById(R.id.editState);
		editPop = (EditText)findViewById(R.id.editPop);
		searchBtn = (Button)findViewById(R.id.searchBtn);
		cancelBtn = (Button)findViewById(R.id.cancelBtn);
		exitBtn = (Button)findViewById(R.id.exitBtn);
		radioGroup = (RadioGroup)findViewById(R.id.radioGroup1);
				
		introTxt.setText("Narrow down records in " + selectedState);
		//radioGroup
		
		searchBtn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String cityIncludes = "";
				String popNum = "";
				
				if(!editState.getText().toString().equals("")){
					cityIncludes = editState.getText().toString();
					bundle.putString("cityIncludes", cityIncludes);
				}else{
					bundle.putString("cityIncludes", "");
				}
				
				if(!editPop.getText().toString().equals("")){
					popNum = editPop.getText().toString();
					bundle.putString("popNum", popNum);
					
					int selectedId = radioGroup.getCheckedRadioButtonId();
					selectedRadio = (RadioButton)findViewById(selectedId);
					
					if(selectedRadio.getText().equals("is greater than")){//greater
						bundle.putString("popMode", "greater");
					}else if(selectedRadio.getText().equals("is less than")){//less
						bundle.putString("popMode", "less");
					}
										
				}else{
					bundle.putString("popNum", "");
				}
				
				intent.putExtras(bundle);
				setResult(2, intent);
	    		finish();	
			}
		});		
		
		cancelBtn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
	    		//setResult(Activity.RESULT_OK, intent);
	    		//finish();
	    		onBackPressed();
			}
		});	
		
		exitBtn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder dialog = new AlertDialog.Builder(myNarrowDownActivity.this);
				dialog.setPositiveButton("YES", new OnClickListener(){

					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub					
						dialog.dismiss();
						setResult(Activity.RESULT_CANCELED, intent);
						finish();
					}
					
				});
				
				dialog.setNegativeButton("NO", new OnClickListener(){

					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
					
				});
				
				dialog.setTitle("Exit Application");
				dialog.setMessage("Are you sure you want to exit?");
				dialog.show();
			}
		});	
		
	}
	
	public void onBackPressed() {
		setResult(Activity.RESULT_OK, intent);
		finish();
	}
	
}
