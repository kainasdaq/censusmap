package neu.KaiDeng;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class SplashScreenActivity extends Activity{
	private static int SPLASH_TIME_OUT = 4000;
	
	private ImageView mImageView;
	//private GLSurfaceView mGLView;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// requesting to turn the title OFF
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// making it full screen
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_splash);
		//mGLView = new MyGLSurfaceView(this);
        //setContentView(mGLView);
		
		double scale = 2.0;
		resizeLogo(scale);
				
		new Handler().postDelayed(new Runnable() {			 
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);		
	}
	
	public void resizeLogo(double d){
		float scaleWidth = 1;
		float scaleHeight = 1;
			
		Bitmap bmp = BitmapFactory.decodeResource(this.getResources(), R.drawable.census_logo);
		int bmpWidth = bmp.getWidth();
	    int bmpHeight = bmp.getHeight();
	      
	    scaleWidth = (float)(scaleWidth*d);	    
	    scaleHeight = (float)(scaleHeight*d);
	    Matrix matrix = new Matrix();
	    matrix.postScale(scaleWidth, scaleHeight);
	    
	    Bitmap resizedBmp = Bitmap.createBitmap(bmp, 0, 0, bmpWidth, bmpHeight, matrix, true);

        mImageView=(ImageView)findViewById(R.id.imgLogo);
        mImageView.setImageBitmap(resizedBmp);
        
	}
}
