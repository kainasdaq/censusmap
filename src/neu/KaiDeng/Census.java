package neu.KaiDeng;

public class Census {
	public String statePostal, placenameFull, lng, lat, pop, totSqMi, popSqMi, pctHisp, pctWhite, pctBlack, pctAsian;
	
	Census(){
		this.statePostal = "";
		this.placenameFull = "";		
		this.lng = "0";
		this.lat = "0";
		this.pop = "0";
		this.totSqMi = "0";
		this.popSqMi = "0";
		this.pctHisp = "0";
		this.pctWhite = "0";
		this.pctBlack = "0";
		this.pctAsian = "0";
	}

}
