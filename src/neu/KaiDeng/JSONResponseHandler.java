package neu.KaiDeng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JSONResponseHandler implements ResponseHandler<List<Census>> {

	public List<Census> handleResponse(HttpResponse arg)
			throws ClientProtocolException, IOException {
		// TODO Auto-generated method stub
		List<Census> cList = new ArrayList<Census>();
		String jsonResponse = new BasicResponseHandler().handleResponse(arg);

		try {
			JSONObject jsonObject = (JSONObject) new JSONTokener(jsonResponse)
					.nextValue();
			JSONArray jsonArray = jsonObject.getJSONArray("response");
			// cList.clear();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject tmp_json = (JSONObject) jsonArray.get(i);
				Census tmp_c = new Census();

				tmp_c.placenameFull = checkPlacename((String)tmp_json.get("PlacenameFull"));
				tmp_c.lng = String.valueOf(tmp_json.get("Long"));
				tmp_c.lat = String.valueOf(tmp_json.get("Lat"));
				tmp_c.pop = String.valueOf(tmp_json.get("Pop"));
				tmp_c.totSqMi = String.valueOf(tmp_json.get("TotSqMi"));
				tmp_c.popSqMi = String.valueOf(tmp_json.get("PopSqMi"));
				tmp_c.pctHisp = String.valueOf(tmp_json.get("PctHisp"));
				tmp_c.pctWhite = String.valueOf(tmp_json.get("PctWhite"));
				tmp_c.pctBlack = String.valueOf(tmp_json.get("PctBlack"));
				tmp_c.pctAsian = String.valueOf(tmp_json.get("PctAsian"));
				
				if(tmp_c.pctHisp.equals("")){
					tmp_c.pctHisp = "0";
				}
				if(tmp_c.pctWhite.equals("")){
					tmp_c.pctWhite = "0";
				}
				if(tmp_c.pctBlack.equals("")){
					tmp_c.pctBlack = "0";
				}
				if(tmp_c.pctAsian.equals("")){
					tmp_c.pctAsian = "0";
				}
				
				cList.add(tmp_c);
				//System.out.println(tmp_c.placenameFull);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cList;
	}
	
	private String checkPlacename(String s){
		char[] cArray = s.toCharArray();
		for(int i = 0; i < cArray.length; i++){
			if(cArray[i] == '\''){
				cArray[i] = ' ';
			}
		}
		s = String.valueOf(cArray);
		
		String regEx = "\\w+\\s\\w+";//regular expression for city name
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(s);
		if(m.find()){
			//System.out.println(m.group());
			s = m.group();
		}
		return s;		
	}

}
