package neu.KaiDeng;

import neu.KaiDeng.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends Activity {
    /** Called when the activity is first created. */
	final private String[] STATE_LIST = {
			"AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", 
			"GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", 
			"MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", 
			"NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", 
			"SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", 
			"WY"};
	Spinner spin;
	Button updateBtn;
	Button viewBtn;
	Button exitBtn;
	Bundle bundle = new Bundle();
	
	String selectedState = STATE_LIST[0];;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        spin = (Spinner)findViewById(R.id.state_spinner);
        updateBtn = (Button)findViewById(R.id.updateBtn);
        viewBtn = (Button)findViewById(R.id.viewBtn);
        exitBtn = (Button)findViewById(R.id.exitBtn);
        
        ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, STATE_LIST);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);       
        spin.setOnItemSelectedListener(new OnItemSelectedListener(){

			public void onItemSelected(AdapterView<?> parent, View v,
					int pos, long id) {
				// TODO Auto-generated method stub
				selectedState = STATE_LIST[pos];
			}

			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				selectedState = STATE_LIST[0];
			}
        	
        });
        
        updateBtn.setOnClickListener(new OnClickListener(){

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				bundle.putBoolean("updateFlag", true);
				goToTable();
			}
        	
        });
        
        viewBtn.setOnClickListener(new OnClickListener(){

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				bundle.putBoolean("updateFlag", false);
				goToTable();
			}
        	
        });
        
        exitBtn.setOnClickListener(new OnClickListener(){

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				exitApp();
			}
        	
        });
        
    }
    
    private void goToTable(){
		bundle.putString("state", selectedState);
		Intent toTable = new Intent(this, myTableActivity.class);
		toTable.putExtras(bundle);
		startActivityForResult(toTable, 0); 
    }
    
	protected void onActivityResult(int requestCode, int resultCode, Intent intent){
    	super.onActivityResult(requestCode, resultCode, intent);
    	switch(requestCode){
    	case 0://update entry
    		if(resultCode == Activity.RESULT_CANCELED){
    			finish();
    		}
    		break;
    	}
	}
    
    public void exitApp(){
    	AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		
		dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				finish();
			}
		});


		dialog.setNegativeButton("NO", new DialogInterface.OnClickListener(){

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
			
		});
		
		dialog.setTitle("Exit Application");
		dialog.setMessage("Are you sure you want to exit?");
		dialog.show();
    }
    
	public void onBackPressed() {
		exitApp();
	}

}