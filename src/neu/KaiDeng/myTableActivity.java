package neu.KaiDeng;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import neu.KaiDeng.JSONResponseHandler;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class myTableActivity extends Activity{
	//private static final int DIALOG_DOWNLOAD_PROGRESS = 0;
	final String apiUrl = "http://api.usatoday.com/open/census/loc?keypat=";
	final String apiUrl_2 = "&sumlevid=4,6&api_key=";
	final String apiKey = "5hjwpxthjzszexjktjpdgaza";
	
	Intent intent;
	Bundle bundle;
	String selectedState;
	Boolean updateFlag;
	
	ListView listView;
	TextView introTxt;
	Button narrowBtn;
	List<Census> censusList = new ArrayList<Census>();
	List<String> finalList = new ArrayList<String>();
	
	myDB databaseAdapter;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_table);
		
		introTxt = (TextView)findViewById(R.id.introTxt);
		listView = (ListView)findViewById(R.id.firstList);
		narrowBtn = (Button)findViewById(R.id.narrowBtn);
		
		intent = this.getIntent();
		bundle = intent.getExtras();
		selectedState = bundle.getString("state");
		updateFlag = bundle.getBoolean("updateFlag");
		
		databaseAdapter = new myDB(selectedState);

		databaseAdapter.openDB();
		databaseAdapter.createTable();
		boolean isStateExisting = databaseAdapter.checkStateExisting(selectedState);
		databaseAdapter.closeDB();
		
		//*********following 2 parts are different!************
		if(updateFlag){						
			if(!isStateExisting){//the state is NOT existing
				updateOnline();
			}else{//the state is existing, show the list
				AlertDialog.Builder dialog = new AlertDialog.Builder(this);
				dialog.setPositiveButton("YES", new OnClickListener(){

					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub					
						dialog.dismiss();
						updateOnline();
					}
					
				});
				
				dialog.setNegativeButton("NO", new OnClickListener(){

					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						processQuery();
					}
					
				});
				
				dialog.setTitle("Relevant records are found");
				dialog.setMessage("Will you update data from the internet anyway?");
				dialog.show();
			}
		}else{
			if(!isStateExisting){//the state is NOT existing
				AlertDialog.Builder dialog = new AlertDialog.Builder(this);
				dialog.setPositiveButton("YES", new OnClickListener(){

					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub					
						dialog.dismiss();
						updateOnline();
					}
					
				});
				
				dialog.setNegativeButton("NO", new OnClickListener(){

					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
			    		setResult(Activity.RESULT_OK, intent);//go back to 1st screen
			    		finish();
					}
					
				});
				
				dialog.setTitle("Relevant records are NOT found");
				dialog.setMessage("Will you download data from the internet?");
				dialog.show();
			}else{//the state is existing, show the list
				processQuery();
			}
		}
		
		listView.setOnItemClickListener(new OnItemClickListener(){

			public void onItemClick(AdapterView<?> parent, View v, int pos, long id) {
				// TODO Auto-generated method stub
				if(pos != 0){
					String tmp = finalList.get(pos);
					//String regEx = "\\w+\\s\\w+\\,\\s\\w+";//regular expression for city + state
					String regEx = "\\w+\\s\\w+";//regular expression for city name
					Pattern p = Pattern.compile(regEx);
					Matcher m = p.matcher(tmp);
					if(m.find()){
						System.out.println(m.group());
						String city = m.group();	
						
						bundle.putString("city", city);
						bundle.putString("state", selectedState);
						Intent toMap = new Intent(myTableActivity.this, MyMapActivity.class);
						toMap.putExtras(bundle);
						startActivityForResult(toMap, 0);
						
					}
					
				}
			}
			
		});

		narrowBtn.setOnClickListener( new View.OnClickListener(){
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				bundle.putString("state", selectedState);
				Intent toNarrowDown = new Intent(myTableActivity.this, myNarrowDownActivity.class);
				toNarrowDown.putExtras(bundle);
				startActivityForResult(toNarrowDown, 1);
			}
        	
        });
		
	}
	/***************** onCreate ENDS ******************/
	
	private void processQuery(){
		introTxt.setText("Querying the local database...");
		showWaiting();
		finalList = databaseAdapter.processQuery();
    	showList();
    	introTxt.setText("Records found in the local database" + "\nPress menu for options");
	}
	
	private void updateOnline(){
		if(checkInternet(this)){
			introTxt.setText("Updating data online...");
			showWaiting();
			new HttpGetTask().execute(apiUrl + selectedState + apiUrl_2 + apiKey);
		}else{
			makeToast("No internet connection");
		}
		
	}
	
	private void showWaiting(){
		finalList.clear();
		finalList.add("Processing...");
		showList();
	}
	
	private void showList(){
		listView.setAdapter(new ArrayAdapter<String>(this, R.layout.list_item, finalList));
	}
	
	private void makeToast(String s){
	    Toast toast = Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG);
	 	toast.setGravity(Gravity.CENTER, 0, 0);
	 	toast.show(); 
	}
	
	/******************* CHECK INTERNET STARTS *********************/
	public static boolean checkInternet(Context context){
		ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobile = connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);		
		return wifi.isConnected() || mobile.isConnected();		
	}	
	/******************* CHECK INTERNET ENDS *********************/
	
	/******************* HTTP STARTS *********************/
	private class HttpGetTask extends AsyncTask<String, Integer, List<Census>>{
		private String error = "";
		private int lengthOfFile = 0;
		private ProgressDialog pd = new ProgressDialog(myTableActivity.this);
		
		@Override
		protected void onPreExecute(){
			pd.setMessage("Downloading data...");
			//pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			//pd.setProgress(0);
			//pd.setIndeterminate(true);
			pd.setCancelable(false);
			pd.show();
		}
		
		@Override
		protected List<Census> doInBackground(String... params) {
			// TODO Auto-generated method stub
			HttpParams httpParam = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParam, 5000);
			HttpConnectionParams.setSoTimeout(httpParam, 10000);
			HttpClient httpClient = new DefaultHttpClient(httpParam);
			
			//AndroidHttpClient httpClient = AndroidHttpClient.newInstance("");			
			HttpGet httpGet = new HttpGet(params[0]);			
			JSONResponseHandler jsonHandler = new JSONResponseHandler();
			
			try{
				URL url = new URL(params[0]);
				URLConnection connection = url.openConnection();
				connection.connect();
				lengthOfFile = connection.getContentLength();
				publishProgress(lengthOfFile);
				
				return httpClient.execute(httpGet, jsonHandler);//(request, handler)
			} catch(ClientProtocolException e){
				error = e.getMessage();
				e.printStackTrace();
			} catch(IOException e){
				error = e.getMessage();
			}
			/*finally{
				try{
					httpClient.close();
				} catch(Exception e){
					e.printStackTrace();
				}
			}*/
			
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Integer... values){
			int kb = (int)values[0]/1024;
			pd.setMessage("Downloading...\nData size: " + kb + "KB");
			/*pd.setMax(values[0]);
			pd.setProgress(values[1]);
			System.out.println(values[0] + " " + values[1]);*/
		}
		
		@Override
		protected void onPostExecute(List<Census> result){
			pd.dismiss();
			if(error.equals(""))
				onFinishGetRequest(result);	
			else
				makeToast("error");
		} 
	}
	
    private void onFinishGetRequest(List<Census> result){
		censusList = result;
		System.out.println(censusList.size());
    	databaseAdapter.openDB();
    	databaseAdapter.deleteRecords();//delete relevant record first!
    	databaseAdapter.insertList(censusList);
    	finalList = databaseAdapter.queryDB("statePostal='"+selectedState+"'" , null);
    	databaseAdapter.closeDB();
    	showList();
    	introTxt.setText("Online update finished" + "\nPress menu for options");
    }
    
	
	/*@Override
	protected Dialog onCreateDialog(int id){
		ProgressDialog pd = new ProgressDialog(myTableActivity.this);
		switch(id){
		case DIALOG_DOWNLOAD_PROGRESS:
			pd = new ProgressDialog(this);
			pd.setMessage("Downloading data...");
			pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pd.setCancelable(true);
			return pd;
		default:
			return null;
		}	
	}*/
    /******************* HTTP ENDS *********************/
	
	/******************* MENU STARTS *********************/	
	public boolean onCreateOptionsMenu(Menu m) {
		int groupId = 0;
		int order = 0;
		int itemId = 0;
		m.add(groupId, itemId++, order, "Narrow down results >>");
		m.add(groupId, itemId++, order, "Refresh list");	
		m.add(groupId, itemId++, order, "<< Back");		
		m.add(groupId, itemId++, order, "Drop the data table");
		m.add(groupId, itemId++, order, "Exit");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		
		switch (item.getItemId()) {
		case 0://narrow down results			
			bundle.putString("state", selectedState);
			Intent toNarrowDown = new Intent(myTableActivity.this, myNarrowDownActivity.class);
			toNarrowDown.putExtras(bundle);
			startActivityForResult(toNarrowDown, 1);
			break;
			
    	case 1://refresh
    		processQuery();
    		break;
			
    	case 2://back
    		//setResult(Activity.RESULT_OK, intent);
    		//finish();
    		onBackPressed();
    		break;
    		
		case 3://drop table			
			dialog.setPositiveButton("YES", new OnClickListener(){

				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					databaseAdapter.openDB();
					databaseAdapter.dropTable();			
					databaseAdapter.closeDB();
					finalList.clear();
					finalList.add("Drop table successfully!");	
					showList();
					makeToast("Drop table successfully!");
					
					dialog.dismiss();
				}
				
			});
			
			dialog.setNegativeButton("NO", new OnClickListener(){

				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
				
			});
			
			dialog.setTitle("Drop local tables");
			dialog.setMessage("It is unrecoverable!\nAre you sure you want to drop the table?");
			dialog.show();
			break;
			
		case 4://exit app			
			dialog.setPositiveButton("YES", new OnClickListener(){

				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub					
					dialog.dismiss();
					setResult(Activity.RESULT_CANCELED, intent);
					finish();
				}
				
			});
			
			dialog.setNegativeButton("NO", new OnClickListener(){

				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
				
			});
			
			dialog.setTitle("Exit Application");
			dialog.setMessage("Are you sure you want to exit?");
			dialog.show();
			break;
		}
		
			
		return true;
	}
	/******************* MENU ENDS *********************/
	
	protected void onActivityResult(int requestCode, int resultCode, Intent intent){
    	super.onActivityResult(requestCode, resultCode, intent);
    	switch(requestCode){
    	case 0://back from map
    		if(resultCode == Activity.RESULT_CANCELED){
    			setResult(Activity.RESULT_CANCELED, intent);
    			finish();
    		}
    		break;
    		
    	case 1://back from narrow down
    		if(resultCode == Activity.RESULT_CANCELED){
    			setResult(Activity.RESULT_CANCELED, intent);
    			finish();
    		}else if(resultCode == 2){
    			bundle = intent.getExtras();
    			String cityIncludes = bundle.getString("cityIncludes");
    			String popNum = bundle.getString("popNum");
    			String popMode = bundle.getString("popMode");
    			String where = "statePostal='"+ selectedState + "'";
    			String intro = "In " + selectedState + ",";
    			
    			System.out.println("state: " + cityIncludes + "\npopNum: " + popNum + "\npopMode: " + popMode);
    			
    			databaseAdapter.openDB();
    			
    			if(!cityIncludes.equals("")){
    				where += "AND placenameFull like '%" + cityIncludes + "%'"; 
    				intro += " city name includes " + cityIncludes + "/nAND";
    				finalList = databaseAdapter.queryDB(where, null);
    				
    			}
    			if(!popNum.equals("")){
    				String sign = "";
    				if(popMode.equals("greater")){
    					sign = " > ";
    				}else if(popMode.equals("less")){
    					sign = " < ";
    				}
    				
    				where += "AND pop" + sign + popNum; 
    				intro += " population " + sign + " " + popNum;   				
    				finalList = databaseAdapter.queryDB(where, null);
    			}
    			   	    	    	    	
    			databaseAdapter.closeDB();
    	    	showList();
    	    	introTxt.setText(intro);
    		}
    		
    		break;
			
		}
	}
	
	public void onBackPressed() {
		setResult(Activity.RESULT_OK, intent);
		finish();
	}
	
}
